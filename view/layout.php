<!DOCTYPE>
<html lang="ru">
<head>

    <title>TestApp</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS Libs -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="fonts/feather/iconfont.css">
    <link rel="stylesheet" href="css/ui.css">
    <!-- /CSS Libs -->

    <link rel="stylesheet" href="css/style.css">

</head>
<body>

<div class="container">
    <? include 'main' . DS . 'init.php'; ?>
</div>

<!-- JS Libs -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
<!-- /JS Libs -->

<script src="js/app.js"></script>

</body>
</html>