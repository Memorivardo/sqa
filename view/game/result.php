<div class="summary d-flex justify-content-center mb-3">

    <div class="mr-2">
    <?=$results['bots'][0]['name']?>
    </div>
    <div>
        <?=($results['summary'][0] ?? 0)?>
        <span class="ml-1 mr-1">:</span>
        <?=($results['summary'][1] ?? 0)?>
    </div>
    <div class="ml-2">
    <?=$results['bots'][1]['name']?>
    </div>

</div>

<div class="row">
    <? foreach($results['rounds'] as $round => $tries): ?>
    <div class="col-md-4">
        <div class="card h-100">
            <div class="card-header">
                Раунд <?=((int)$round + 1)?>
            </div>
            <div class="card-body">

                <table class="table table-bordered table-condensed">
                    <thead>
                    <tr>
                        <? foreach($results['bots'] as $bot): ?>
                            <th colspan="2">
                                <?=$bot['name']; ?>
                            </th>
                        <? endforeach; ?>
                    </tr>
                    <tr>
                        <? foreach($results['bots'] as $bot): ?>
                            <th>
                                Загадал
                            </th>
                            <th>
                                Отгадал
                            </th>
                        <? endforeach; ?>
                    </tr>
                    </thead>
                    <? foreach($tries as $try): ?>
                        <tr>
                            <? foreach($results['bots'] as $bot_index => $bot): ?>
                            <td class="alert alert-<?=(in_array($bot_index,$try['winners']) ? 'success' : 'danger');?>">
                                <?=$try['ravels'][$bot_index]; ?>
                            </td>
                            <td class="alert alert-<?=(in_array($bot_index,$try['winners']) ? 'success' : 'danger');?>">
                                <?=$try['unravels'][$bot_index]; ?>
                            </td>
                            <? endforeach;?>
                        </tr>
                    <? endforeach; ?>
                </table>


            </div>
        </div>

    </div>
    <? endforeach; ?>
</div>
