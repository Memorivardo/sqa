let App = {
    init: function () {

        let btn = document.getElementById('js-start-game');
        btn.addEventListener('click', function(e) {

            App.ajax();

        }, false);
    },
    ajax: function() {
        let xhr = new XMLHttpRequest();

        xhr.open('POST', '?');
        xhr.setRequestHeader('Content-Type', 'application/json');

        xhr.send(JSON.stringify({
            action: 'start_game'
        }));

        xhr.onload = function() {
            if (xhr.status === 200) {
                var test = JSON.parse(xhr.responseText);

                if(test.html) {
                    let result_block = document.getElementById('result-area');
                    result_block.innerHTML = test.html;
                }
            }
        };

    }


};

App.init();