<?php

namespace Sqa\Traits;

trait HelperTrait
{

    /**
     * @param $message
     * @param array $data
     */
    function error($message, $data = [])
    {
        $this->response([
                'error' => $message
            ] + $data);
    }

    /**
     * @param $message
     * @param array $data
     */
    function success($message, $data = [])
    {
        $this->response([
                'success' => $message
            ] + $data);
    }

    /**
     * @param $data
     */
    function response($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

}