<?php

namespace Sqa\Interfaces;

interface BotInterface
{

    function ravel(int $min, int $max);

    function unravel(int $min, int $max);

}