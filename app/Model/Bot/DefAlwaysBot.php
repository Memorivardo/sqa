<?php

namespace Sqa\Model\Bot;

use Sqa\Interfaces\BotInterface;

class DefAlwaysBot implements BotInterface
{

    private $currentValue = false;

    function ravel(int $min, int $max)
    {
        return rand($min, $max);
    }

    function unravel(int $min, int $max)
    {
        if (empty($this->currentValue)) {
            $this->currentValue = rand($min, $max);
        }

        return $this->currentValue;
    }

    function reset() {
        $this->currentValue = false;
    }
}