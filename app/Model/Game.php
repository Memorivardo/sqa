<?php

namespace Sqa\Model;

use Sqa\Traits\HelperTrait;
use Sqa\Model\Bot\DefAlwaysBot;
use Sqa\Model\Bot\RandAlwaysBot;

class Game
{

    use HelperTrait;

    const DEFAULT_ALWAYS = 1;
    const RANDOM_ALWAYS = 2;

    private $bots = [];
    private $rounds = 1;
    private $min = 0;
    private $max = 100;
    private $results = [];
    private $summary = [];

    function initParticipant($name, $rules)
    {

        if (count($this->bots) > 2) {
            $this->error('Максимальное число игроков - 2');
        }

        $bot = ['name' => $name];

        switch ($rules) {
            case Game::DEFAULT_ALWAYS:
                $bot['bot'] = new DefAlwaysBot();
                break;

            case Game::RANDOM_ALWAYS:
                $bot['bot'] = new RandAlwaysBot();
                break;
        }

        $this->bots[] = $bot;
    }

    function setRoundQuantity($quantity)
    {
        if ($quantity <= 0) {
            $this->error('Количество раундов должно быть положительным числом');
        }

        $this->rounds = $quantity;
    }

    function setMinMax($min, $max)
    {
        if ($min >= $max) {
            $this->error('Минимальное значение должно быть меньше максимального');
        }

        $this->min = $min;
        $this->max = $max;
    }

    function start()
    {
        $this->results = [];
        $this->summary = [0, 0];

        if (count($this->bots) != 2) {
            $this->error('Необходимо 2 игрока для старта игры');
        }

        for ($i = 0; $i < $this->rounds; $i++) {
            do {
                $ravels = [];
                $unravels = [];

                foreach ($this->bots as $key => $bot) {
                    $ravels[$key] = $bot['bot']->ravel($this->min, $this->max);
                    $unravels[$key] = $bot['bot']->unravel($this->min, $this->max);
                }

                $winners = [];
                foreach ($this->bots as $key => $bot) {
                    if ($unravels[$key] == $ravels[count($ravels) - 1 - $key]) {
                        $winners[] = $key;
                    }

                }

                $this->results[$i][] = [
                    'ravels' => $ravels,
                    'unravels' => $unravels,
                    'winners' => $winners
                ];

            } while (count($winners) != 1);

            foreach ($this->bots as $key => $bot) {
                $bot['bot']->reset();
            }

            $this->summary[$winners[0]]++;

        }

    }

    function getLastResults()
    {

        return [
            'bots' => $this->bots,
            'rounds' => $this->results,
            'summary' => $this->summary
        ];
    }
}