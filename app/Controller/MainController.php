<?php


namespace Sqa\Controller;

use Sqa\Core\Controller;
use Sqa\Model\Game;
use Sqa\Core\Tools;

class MainController extends Controller
{

    function start_game()
    {
        $game = new Game();

        $game->initParticipant('Первый бот', Game::RANDOM_ALWAYS);
        $game->initParticipant('Второй бот', Game::DEFAULT_ALWAYS);

        $game->setRoundQuantity(3);
        $game->setMinMax(1, 10);

        $game->start();

        $results = $game->getLastResults();

        $this->response([
            'html' => Tools::get_include_contents(TEMPLATE_DIR . 'game' . DS . 'result.php', [
                'results' => $results
            ])
        ]);
    }


}