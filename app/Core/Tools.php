<?php

namespace Sqa\Core;

class Tools
{
    /**
     * @param $filename
     * @param array $vars
     * @return bool|false|string
     */
    static function get_include_contents(string $filename, $vars = [])
    {
        if (is_file($filename)) {
            ob_start();
            if (!empty($vars)) {
                extract($vars, EXTR_SKIP | EXTR_REFS);
            }

            include $filename;
            return ob_get_clean();
        }
        return false;
    }
}