<?php

namespace Sqa\Core;


class Core
{

    static function init()
    {
        try {

            $route = 'main';

            $controller_name = 'Sqa\\Controller\\' . ucfirst($route) . 'Controller';
            $controller = new $controller_name();
            $controller->render();

        } catch (\Exception $e) {
            die($e->getMessage());
        } catch (\TypeError $e) {
            die($e->getMessage());
        }
    }
}