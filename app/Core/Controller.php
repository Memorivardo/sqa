<?php

namespace Sqa\Core;

use Sqa\Traits\HelperTrait;

class Controller
{
    use HelperTrait;

    protected $template;
    protected $view = [];

    /**
     * Controller constructor.
     * @throws Exception
     */
    function __construct()
    {

        $raw_post = json_decode(file_get_contents('php://input'),true);

        if (!empty($raw_post['action'])) {
            if (method_exists($this, $raw_post['action'])) {
                $this->{$raw_post['action']}();
            } else {
                throw new \Exception('Не найден обработчик для действия ' . $raw_post['action']);
            }
        }

    }

    /**
     * @throws Exception
     */
    function render()
    {
        //If we need to execute some operations before render
        if (method_exists($this, 'init')) {
            $this->init();
        }

        extract($this->view, EXTR_SKIP);

        $template = !empty($this->template) ? str_replace('.', DIRECTORY_SEPARATOR, $this->template) : 'layout';

        ob_start();

        $filename = TEMPLATE_DIR . $template . '.php';
        if (file_exists($filename)) {
            include $filename;
        } else {
            throw new \Exception('Can\'t find template ' . $template);
        }

        die(ob_get_clean());
    }


}